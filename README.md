# viem lo tuyen co tu cung

<p>Đốt điện là một trong những biện pháp điều trị viêm lộ tuyến cổ tử cung hiệu quả. Trước khi những biện pháp mới được đưa vào như áp lạnh, laser hay ALA-PDT thì đốt điện được áp dụng khá rộng rãi tại các cơ sở y tế, đặc biệt là các bệnh viện công của nhà nước.&nbsp;</p>

<p>Viêm lộ tuyến cổ tử cung là bệnh thường gặp ở các chị em trong độ tuổi sinh sản.&nbsp;</p>

<p>Viêm lộ tuyến cổ tử cung là căn bệnh lành tính, thường chỉ cần điều trị nội khoa bằng thuốc theo chỉ định của bác sĩ là có thể khỏi.&nbsp;</p>

<p>Tuy nhiên, với các trường hợp viêm nhiễm nặng, bệnh dễ tái phát thì cần áp dụng thêm biện pháp vật lý trị liệu như đốt điện, áp lạnh hoặc laser.</p>


